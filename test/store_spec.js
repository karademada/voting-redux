import { expect } from 'chai';
import { fromJS } from 'immutable';

import makeStore from '../src/store';

import reducer from '../src/reducer';

// TODO make it works with jest

describe('Store', () => {
	it('is redux store configured with the correct reducer', () => {
		let store = makeStore(reducer);

		store.dispatch({
			type: 'SET_ENTRIES',
			entries: ['Trainspotting', '28 Days Later'],
		});

		expect(store.getState()).to.equal(fromJS({
			entries: ['Trainspotting', '28 Days Later'],
		}));
	});
});
