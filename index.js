import makeStore from './src/store';
import startServer from './src/server';

const store = makeStore();
startServer(store);

const entries = require('./entries.json');

store.dispatch({
  type: 'SET_ENTRIES',
  entries,
});
store.dispatch({ type: 'NEXT' });
